# Exit Intent PopUp

A VueJs component to trigger and display promotional popups at a given duration, action or exit intent.

# [Try the demo](https://eloquent-swartz-e4b0b6.netlify.app/)

## Project setup
Besides the PopUp component the project includes a showcase demo app. Build it locally by
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

## Example
```vue
<template>
<!-- Show PromotionalCard after either 30 seconds or scrolling 60% of the page -->
    <PopUp
      :show-after-seconds="30" 
      :show-after-scrolling="60"
      :display-frequency-in-days="3"
    ><PromotionalCard></PromotionalCard>
    </PopUp>

<!-- Show PromotionalCard on Exit Intent after the user stayed for at least 30 seconds -->
    <PopUp
      :show-on-exit-intent="true"
      :exit-intent-delay="30"
      :display-frequency-in-days="1"
    ><PromotionalCard></PromotionalCard>
    </PopUp>
</template>

<script>
  import PopUp from "./components/PopUp";
  import PromotionalCard from "./components/PromotionalCard";
  export default {
    components: { 
        PopUp,
        PromotionalCard
    }
  }
</script>
```
## API
### Props
| Property               | Description                                                                                     |  type   | default |
| ---------------------- | ----------------------------------------------------------------------------------------------- | :-----: | :-----: |
| name                   | A unique name identifying the popup                                                             | String  |  null   |
| showAfterSeconds       | After how many seconds to automatically display the popup (null to disable)                     | Number  |  null   |
| showAfterSrolling      | Percent to scroll to display the PopUp (Null to disable)                                        | Number  |  null   |
| showOnExitIntent       | Whether to enable the exit intent trigger                                                       | Boolean |  false  |
| exitIntentDelay        | Delay in seconds to enable the exit intent trigger                                              | Number  |    0    |
| mobileScrollThreshold  | Percentage of the page to scroll up on Mobile to trigger an exit intent                         | Number  |    7    |
| displayFrequencyInDays | How long the popup will stay dormant until triggering again for the same user (0 for everytime) | Number  |    0    |

### Events
| Event name | Description                                 |                    Emitted Value                    |
| ---------- | ------------------------------------------- | :-------------------------------------------------: |
| shown      | Emitted when the popup is shown to the user |                          -                          |
| hidden     | Emitted when the popup is closed            | {nextDate: The next date that the popup will show}] |
| activated  | Emitted when the popup is mounted           | {nextDate: The next date that the popup will show}] |

